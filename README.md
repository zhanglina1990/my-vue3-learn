# my-vue3-learn

这是一个基于 Vue 3 的学习项目，用于记录和分享我的学习过程。

# 技术栈
* "ant-design-vue": "4.x",
* "pinia": "^2.3.1",
* "vue": "^3.5.13",
* "vue-router": "^4.5.0"
* "vite": "^6.0.11",
* "sass": "^1.83.4",
* "typescript": "~5.7.3",

## 开发

```sh
pnpm install
```

### Compile and Hot-Reload for Development

```sh
pnpm dev
```

### Type-Check, Compile and Minify for Production

```sh
pnpm build
```
