import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/home/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'index',
      redirect: '/home',
    },{
      path: "/home",
      name: "home",
      component: Home,
      children: [
        {
          path: 'homeLayout',
          name: '首页',
          component: () => import('@/views/home/HomeView.vue')
        },
        {
          path: 'vue01',
          name: '第一节',
          component: () => import('@/views/vue01/index.vue')
        },
        {
          path: 'vue02',
          name: '第二节',
          component: () => import('@/views/vue02/index.vue')
        }
      ]
    }
  ],
})

export default router
