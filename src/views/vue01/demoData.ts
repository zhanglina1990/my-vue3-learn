// 表格数据
// 生成随机数据
const buildData = (size:number) => {
    let data = [];
    for (let i = 0; i < size; i++) {
        data.push({
            key: i,
            name: `张小${i}`,
            sex: Math.random() > 0.5 ? '男' : '女',
            age: Math.floor(Math.random() * 30),
            address: `西湖区湖底公园 ${i} 号`,
        });
    }
    return data;
}


export const tableData = buildData(20);
export const tableColumn = [
    {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
    },
    {
        title: '年龄',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: '住址',
        dataIndex: 'address',
        key: 'address',
    },
];
